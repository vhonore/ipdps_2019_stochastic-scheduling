# Copyright (c) 2018 Univ. Bordeaux



import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib import rc
from copy import *
import numpy as np
from cmath import *
import scipy
import json
from matplotlib.lines import Line2D
import random


from settings import *
import distri
from tools import *
from algorithms import *

rc('text', usetex=True)






########################## GENERAL PARAMETERS ##########################


# List of distributions to plot #
list_distributions = ["Exponential","Weibull","Gamma","Lognormal","TruncatedNormal","Pareto","Uniform","Beta","BoundedPareto"]


# Global variables imported from settings


init_settings()
init_distributions()

beta = settings.beta
nX = settings.nX
nb_chunks = settings.nb_chunks
precision = settings.precision
pthres = settings.pthres




qq=1 #choosing if we are going with quantiles or times for t1.
if(qq):
	list_t = settings.list_q
else:
	list_t = settings.list_t



# function to update tmax of distributions with a new quantile
def update_quantile(quantile):
    settings.pthres = quantile
    settings.max_exponential = quant_exponential(quantile)
    settings.max_weibull = quant_weibull(quantile)
    settings.max_gamma = quant_gamma(quantile)
    settings.max_lognormal = quant_lognormal(quantile)
    settings.max_truncatednormal = quant_truncatednormal(quantile)
    settings.max_pareto = quant_pareto(quantile)
    settings.max_uniform = b_uniform
    settings.max_beta = 1.0
    settings.max_boundedpareto = H_boundedpareto



########################## END GENERAL PARAMETERS ##########################


































########################## START TEST SELECTION ##########################


generates_tables_data = True # if true, generate the tables of the paper, in separated .tex files
generates_pareto_precision_discretization = True # if true, plot the experiment of changing the precision of quantile for computing tmax and show the effect on discretization schemes
generates_HPC_simulation_data =  True # generate the data files (.txt) of the plots of HPC evaluation section (V-C)
generates_HPC_plots_from_data = True # generate the plots associated to the data of .txt above (we provide the paper data as a baseline)


########################## END TEST SELECTION ##########################


































########################## START SIMULATION ##########################








###### Start Plot of pareto discretization with regards to precision of the quantile (shows possible bad performances) #####


if(generates_pareto_precision_discretization):

	print("\n\n Generating Pareto bad discretization performance...\n")

	max_precision = 16
	min_precision = 2
	distrib = "Pareto"

	alg = dynamic_programming_discrete
	r_optT =[] # store results for Equal-Time scheme
	r_optP = [] # store results for Equal-Probability scheme



	quantile = []
	for i in range(max_precision,min_precision-1,-1):
		assert(i>0)
		quantile.append(1-pow(10,-i))


	for q in quantile:
		update_quantile(q)
		r_optT.append(monte_carlo_simulation(alg, distrib, linear, invlinear , mean_pareto, nX, 500, 1)/mean_pareto)
		r_optP.append(monte_carlo_simulation(alg, distrib, linear, invlinear , mean_pareto, nX, 500, 0)/mean_pareto)
		
	update_quantile(0.99999999)


	plt.figure()
	plt.plot([i for i in range(max_precision,min_precision-1,-1)], r_optT , 'blue', lw=3, alpha=0.6,label=r'\sc Equal-Time')
	plt.plot([i for i in range(max_precision,min_precision-1,-1)], r_optP , 'red', lw=3, alpha=0.6,label=r'\sc Equal-Prob')
	plt.legend(loc='best', frameon=False, fontsize=12)
	plt.tick_params(axis='both', which='major', labelsize=18)
	plt.xlabel(r'Precision $p$ for Distribution Truncation ($10^{-p}$)',fontsize=18)
	plt.ylabel('Normalized Cost',fontsize=18)
	axes = plt.gca()
	plt.savefig(getattr(settings,'path')+"quantile_variation_discretization_pareto.pdf",orientation='landscape')
	plt.close()


###### End Plot of pareto discretization with regards to precision of the quantile (shows possible bad performances) #####


































###### Start Generation of tables ######


# Generates Table II,III and IV of the paper with current distribution instanciations of file distri.py
if (generates_tables_data):
	###  Write structure of latex file for heuristics table ####

	tab_heuristics = open(getattr(settings,'path')+'tab_heuristics.tex','w')

	tab_heuristics.write("\\begin{tabular}{|c||c|c|c|c|c|c|c|}\n\\hline\n")
	tab_heuristics.write("\\multirow{2}{*}{Distribution} & \\multirow{2}{*}{\\bruteForce} & \\multirow{2}{*}{\\meanBmean} & \\multirow{2}{*}{\\mstdv} & \\multirow{2}{*}{\\mdbl} & \\multirow{2}{*}{\\medBmed} & \\multirow{2}{*}{\\eqT} & \\multirow{2}{*}{\\eqP}\\\\ \n")
	tab_heuristics.write("\\multicolumn{1}{|c||}{} & \\multicolumn{1}{c|}{} & \\multicolumn{1}{c|}{}& \\multicolumn{1}{c|}{}& \\multicolumn{1}{c|}{}& \\multicolumn{1}{c|}{}& \\multicolumn{1}{c|}{} & \\multicolumn{1}{c|}{} \\\\ \n \hline\n")

	#### Write structure for discretization table ####
	tab_discretization = open(getattr(settings,'path')+'tab_discretization.tex','w')

	tab_discretization.write("\\begin{tabular}{|c||")
	for _ in range(0,len(nb_chunks)):
		tab_discretization.write("c|")
	tab_discretization.write("|")
	for _ in range(0,len(nb_chunks)):
		tab_discretization.write("c|")
	tab_discretization.write("}\n\\hline\n")
	tab_discretization.write("\\multirow{2}{*}{Distribution} & \\multicolumn{"+str(len(nb_chunks))+"}{c||}{\\eqT} & \\multicolumn{"+str(len(nb_chunks))+"}{c|}{\\eqP} \\\\ \n")
	tab_discretization.write("\cline{2-"+str(2*len(nb_chunks)+1)+"}\n & ")
	for k in range(0,len(nb_chunks)):
			tab_discretization.write("$n="+str(nb_chunks[k])+"$ & ")
	for k in range(0,len(nb_chunks)-1):
			tab_discretization.write("$n="+str(nb_chunks[k])+"$ & ")
	tab_discretization.write(str(nb_chunks[len(nb_chunks)-1])+" \\\\ \n \hline \n")


	#### Write structure for different t1 table ####

	tab_t1_opt = open(getattr(settings,'path')+'tab_test_t1.tex','w')


	tab_t1_opt.write("\\begin{tabular}{|c||c||")
	for _ in list_t: 
		tab_t1_opt.write("c|")
	tab_t1_opt.write("}\n\\hline\n")
	tab_t1_opt.write("\\multirow{2}{*}{Distribution} & \\multirow{2}{*}{\\tbf{1} (assoc. cost)} & \\multicolumn{"+str(len(list_t))+"}{c|}{Other values of $t_1$ (associated cost)} \\\\ \n")
	tab_t1_opt.write(" & &")
	if(qq):
		for k in range(0,len(list_t)-1):
			tab_t1_opt.write("$Q("+str(list_t[k])+")$ & ")
		tab_t1_opt.write("$Q("+str(list_t[len(list_t)-1])+")$ \\\\ \n \hline \n")
	else:
		for k in range(0,len(list_t)-1):
			tab_t1_opt.write(str(list_t[k])+"\mean & ")
		tab_t1_opt.write(+str(list_t[len(list_t)-1])+"\mean \\\\ \n \hline \n")



	vect_waitingTimeFunction = [linear]
	vect_invWaitingTimeFunction = [invlinear]


	print("\n\n******* START SIMULATION PROCESS *******\n\n\n\n\n")
	print("### ALGORITHM PERFORMANCE SIMULATIONS OVER USUAL DISTRIBUTIONS ###\n\n")
	## Plotting ##
	for distrib in list_distributions:
		print(distrib)
		
		min_distrib =  getattr(distri,"min_"+distrib.lower())
		max_distrib = getattr(settings,"max_"+distrib.lower())
		mean_distrib = getattr(distri,"mean_"+distrib.lower())

		# List of algorithms: OptimalSolution is determining t1 with brute force. Opt-mean test the optimal recurrence with the values of list_t1*mean_distrib as t1

		algorithms = [mean_by_mean, mean_variance, mean_doubling, medium_by_medium,  optimal_recurrence_stochastic,dynamic_programming_discrete,dynamic_programming_discrete]
		algorithms_str = ['Mean-by-Mean','Mean-Stdev','Mean-Doubling', 'Median-By-Median' , 'Opt-Mean', 'Equal-Time','Equal-Probability']
		

		# list of t1 we will test with Optimal recurrence scheme
		if(qq):
			list_t1 = [(globals()["quant_"+distrib.lower()](i)) for i in list_t]
		else:
			list_t1 = [i*mean_distrib for i in list_t]



			
		# simulate all the algorithms for current distribution
		

		# normalization by optimal offline solution
		normalization = mean_distrib

		# brute-force solution
		t1Max= min(max_distrib,t1_max(distrib))
		t1opt,costopt = brute_force_simulation(optimal_recurrence_stochastic,distrib,vect_waitingTimeFunction[0], vect_invWaitingTimeFunction[0],t1Max,nX)
		print(t1opt)
	
		res = []
		for i in range(0,len(algorithms)):
			print("     "+algorithms_str[i])
			algorithm = algorithms[i]
			if(algorithms_str[i]=="Opt-Mean"):
				p = []
				for value in list_t1:
					p.append(monte_carlo_simulation(algorithm, distrib, vect_waitingTimeFunction[0], vect_invWaitingTimeFunction[0],value,nX))
				res.append(p)
			elif(algorithms_str[i]=="Equal-Probability"):
				r = []
				for numberp in nb_chunks:
					r.append(monte_carlo_simulation(algorithm, distrib, vect_waitingTimeFunction[0], vect_invWaitingTimeFunction[0], mean_distrib, nX, numberp, 0))
				res.append(r)
			elif(algorithms_str[i]=="Equal-Time"):
				r = []
				for numberp in nb_chunks:
					r.append(monte_carlo_simulation(algorithm, distrib, vect_waitingTimeFunction[0], vect_invWaitingTimeFunction[0], mean_distrib, nX, numberp, 1))
				res.append(r)
			else:
				res.append(monte_carlo_simulation(algorithm, distrib, vect_waitingTimeFunction[0], vect_invWaitingTimeFunction[0] ,mean_distrib, nX))

		
		# Write in the table heuristics
		tab_heuristics.write(str(distrib)+" & $"+str(format(costopt/normalization,'.2f'))+"$ & $"+str(format(res[0]/normalization,'.2f'))+str("~(")+str(format(res[0]/costopt,'.2f'))+")$ & $"+str(format(res[1]/normalization,'.2f'))+str("~(")+str(format(res[1]/costopt,'.2f'))+")$ & $"+str(format(res[2]/normalization,'.2f'))+str("~(")+str(format(res[2]/costopt,'.2f'))+")$ & $"+str(format(res[3]/normalization,'.2f'))+str("~(")+str(format(res[3]/costopt,'.2f'))+")$ & $"+str(format(res[5][len(nb_chunks)-1]/normalization,'.2f'))+str("~(")+str(format(res[5][len(nb_chunks)-1]/costopt,'.2f'))+")$ & $"+str(format(res[6][len(nb_chunks)-1]/normalization,'.2f'))+str("~(")+str(format(res[6][len(nb_chunks)-1]/costopt,'.2f'))+")$ \\\\ \n \\hline \n")
		# Write in the t_1 table
		tab_t1_opt.write(str(distrib)+" & $"+str(format(t1opt,'.2f')+"~( "+str(format(costopt/normalization,'.2f'))+")$ & "))
		for k in range(0,len(list_t1)-1):
			tab_t1_opt.write("$"+str(format(list_t1[k],'.2f'))+"~("+str(format(res[4][k]/normalization,'.2f'))+")$  & ")
		tab_t1_opt.write("$"+str(format(list_t1[len(res[4])-1],'.2f'))+"~("+str(format(res[4][len(res[4])-1]/normalization,'.2f'))+")$ \\\\ \n \hline \n")
		# Write in the discretization table
		tab_discretization.write(str(distrib)+" & ")
		for k in range(0,len(nb_chunks)):
			tab_discretization.write(str(format(res[5][k]/normalization,'.2f'))+" & ")
		for k in range(0,len(nb_chunks)-1):
			tab_discretization.write(str(format(res[6][k]/normalization,'.2f'))+" & ")
		tab_discretization.write(str(format(res[6][len(res[6])-1]/normalization,'.2f'))+" \\\\ \n \hline \n")
		
		
	# write the end of tabular	
	tab_heuristics.write("\\end{tabular} \n")
	tab_t1_opt.write("\\end{tabular} \n")
	tab_discretization.write("\\end{tabular}\n")

	# close python streams
	tab_heuristics.close()	
	tab_discretization.close()
	tab_t1_opt.close()

	## substitute inf by -
	f = open(getattr(settings,'path')+'tab_test_t1.tex','r')
	filedata = f.read()
	f.close()

	newdata = filedata.replace("inf","-")

	f = open(getattr(settings,'path')+'tab_test_t1.tex','w')
	f.write(newdata)
	f.close()


###### End Generation of tables ######


































###### START Generates data of HPC experiment section with Intrepid data ######



print("\n\n\n\n\n### SIMULATION WITH NEUROSCIENCE APPLICATION TRACES ###\n\n")




wtF = [linear_interpolation_409,quadratic_interpolation_409] # list of waiting time functions to test
invwtF = [invlinear_interpolation_409,quadratic_interpolation_409] # same but with the inverse of the waiting time functions above
wtF_str=["linear","quadratic"] # string representation of waiting time functions
algorithms = [mean_by_mean, mean_variance, mean_doubling, medium_by_medium,dynamic_programming_discrete,dynamic_programming_discrete] # list of algorithms to evaluate
algorithms_str = ['Mean-by-Mean','Mean-Variance','Mean-Doubling', 'Median-By-Median' ,'Equal-Time','Equal-Probability'] # string list of algorithms to evaluate

nb_iterations = 30 # number of points to generate --> number of factors of mean (resp. stdev) from 1 to nb_iterations
vect_mean = [i for i in range(1,nb_iterations+1,1)] # generate the factor vector



assert (len(wtF)==len(invwtF)==len(wtF_str))



### compute the performances for other algorithms when mean varies and stdev is the original one
if generates_HPC_simulation_data:
	nb_chunks = 500
	settings.beta = 1.0
	distrib = "Lognormal"
	res = []
	for j in range (0,len(wtF)):
		print("Generating data of algorithms for Intrepid interpolation, "+str(wtF_str[j])+" 409 WT on distribution 2 with different mean")
		r =[]
		
		p = []

		## Cost of Optimal Solution
		print("  - Algorithm = BRUTE-FORCE")
		for mu in vect_mean:
			print("     Mean --> mu = "+str(mu)+"mu")
			stdev = 258.261/getattr(settings,"normalization_time")
			distri.mean_lognormal = (mu*1253.370)/(getattr(settings,"normalization_time"))
			distri.variance_lognormal = pow(stdev,2) 
			
			distri.nu_lognormal =  log(getattr(distri,"mean_lognormal")/sqrt(pow(stdev/getattr(distri,"mean_lognormal"),2)+1))
			distri.kappa2_lognormal = sqrt(log(pow(stdev/getattr(distri,"mean_lognormal"),2)+1)) 
			distri.min_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*0.00000000000001-1.0)*sqrt(2.0*getattr(distri,"kappa2_lognormal"))))
			settings.max_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*settings.pthres-1.0)*sqrt(2.0*getattr(distri,"kappa2_lognormal"))))
			
			# cost of omniscient scheduler
			assert(settings.beta==1)
			normalization = wtF[j](distri.mean_lognormal) + settings.beta*distri.mean_lognormal

			t1Max= t1_max(distrib)
			t1opt,costopt = brute_force_simulation(optimal_recurrence_stochastic,"Lognormal",wtF[j], invwtF[j],t1Max,nX,-1,0)
			p.append(costopt/normalization)


		## Cost other algorithms
		for i in range(0,len(algorithms)):
			print("  - Algorithm = "+algorithms_str[i])
			q =[]
				
			cpt=0
			for mu in vect_mean:
				print("     Mean --> mu = "+str(mu)+"mu")
				stdev = 258.261/getattr(settings,"normalization_time")
				distri.mean_lognormal = (mu*1253.370)/(getattr(settings,"normalization_time"))
				distri.variance_lognormal = pow(stdev,2) 
			
				distri.nu_lognormal =  log(getattr(distri,"mean_lognormal")/sqrt(pow(stdev/getattr(distri,"mean_lognormal"),2)+1)) 
				distri.kappa2_lognormal = sqrt(log(pow(stdev/getattr(distri,"mean_lognormal"),2)+1)) 
				distri.min_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*0.00000000000001-1.0)*sqrt(2.0*getattr(distri,"kappa2_lognormal"))))
				settings.max_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*settings.pthres-1.0)*sqrt(2.0*getattr(distri,"kappa2_lognormal"))))
			
				# cost of omniscient scheduler
				normalization = wtF[j](getattr(distri,"mean_lognormal")) + settings.beta*getattr(distri,"mean_lognormal")

				algorithm = algorithms[i]
				if(algorithms_str[i]=="Equal-Probability"):
					q.append(monte_carlo_simulation(algorithm, "Lognormal", wtF[j], invwtF[j], -1, nX, nb_chunks, 0)/normalization )
				elif(algorithms_str[i]=="Equal-Time"):
					q.append(monte_carlo_simulation(algorithm, "Lognormal", wtF[j], invwtF[j], -1, nX, nb_chunks, 1)/normalization)
				else:
					q.append(monte_carlo_simulation(algorithm, "Lognormal", wtF[j], invwtF[j], -1, nX)/normalization)
				cpt+=1
			r.append(q)
		res.append(r)

	print(res)


	### save list of algorithm performance for all considered waiting time functions when mean is shiffted by a given factor ###

	f = open(getattr(settings,'path')+"plot_mean.txt", 'w')
	json.dump(res, f)
	f.close()






	### compute the performances for other algorithms when stand. dev. varies and mean is the same
	print("\n\n")

	res2 = []


	for j in range (0,len(wtF)):
		print("Generating data of algorithms for Intrepid interpolation, "+str(wtF_str[j])+" 409 WT on distribution 2 with different standard derivation")
		r = []
		p = []
		## Cost of Optimal Solution
		print("  - Algorithm = BRUTE-FORCE")
		for mu in vect_mean:
			print("     Standard derivation --> sigma = "+str(mu)+"sigma")
			stdev = (mu*258.261)/(getattr(settings,"normalization_time"))
			distri.mean_lognormal = (1253.370)/(getattr(settings,"normalization_time"))
			distri.variance_lognormal = pow(stdev,2)
			
			distri.nu_lognormal =  log(getattr(distri,"mean_lognormal")/sqrt(pow(stdev/getattr(distri,"mean_lognormal"),2)+1)) 
			distri.kappa2_lognormal = sqrt(log(pow(stdev/getattr(distri,"mean_lognormal"),2)+1))
			distri.min_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*0.00000000000001-1.0)*sqrt(2.0*getattr(distri,"kappa2_lognormal"))))
			settings.max_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*settings.pthres-1.0)*sqrt(2.0*getattr(distri,"kappa2_lognormal"))))
			
			# cost of omnisicient scheduler
			normalization = wtF[j](distri.mean_lognormal) + settings.beta*distri.mean_lognormal
			
			t1Max= t1_max(distrib)
			t1opt,costopt = brute_force_simulation(optimal_recurrence_stochastic,distrib,wtF[j], invwtF[j],t1Max,nX,-1,0)
			p.append(costopt/normalization)


		for i in range(0,len(algorithms)):
			print("  - Algorithm = "+algorithms_str[i])
			p = []
			for mu in vect_mean:
				print("     Standard derivation --> sigma = "+str(mu)+"sigma")
				stdev = (mu*258.261)/(getattr(settings,"normalization_time"))
				distri.mean_lognormal = (1253.370)/(getattr(settings,"normalization_time"))#  exp(getattr(distri,"nu_lognormal") + (getattr(distri,"kappa2_lognormal")/2))
				distri.variance_lognormal = pow(stdev,2) #(exp(getattr(distri,"kappa2_lognormal"))-1)*(exp(2*getattr(distri,"nu_lognormal")+getattr(distri,"kappa2_lognormal")))
			
				distri.nu_lognormal =  log(getattr(distri,"mean_lognormal")/sqrt(pow(stdev/getattr(distri,"mean_lognormal"),2)+1)) #(mu*1253.370)/(getattr(settings,"normalization_time"))
				distri.kappa2_lognormal = sqrt(log(pow(stdev/getattr(distri,"mean_lognormal"),2)+1)) #pow(258.261/(getattr(settings,"normalization_time")),2)
				distri.min_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*0.00000000000001-1.0)*sqrt(2.0*getattr(distri,"kappa2_lognormal"))))
				settings.max_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*settings.pthres-1.0)*sqrt(2.0*getattr(distri,"kappa2_lognormal"))))
			
				# cost of ominiscient scheduler
				normalization = wtF[j](distri.mean_lognormal) + settings.beta*distri.mean_lognormal

				algorithm = algorithms[i]
				if(algorithms_str[i]=="Equal-Probability"):
					p.append(monte_carlo_simulation(algorithm, "Lognormal", wtF[j], invwtF[j], -1, nX, nb_chunks, 0) / normalization)
				elif(algorithms_str[i]=="Equal-Time"):
					p.append(monte_carlo_simulation(algorithm, "Lognormal", wtF[j], invwtF[j], -1, nX, nb_chunks, 1) / normalization)
				else:
					p.append(monte_carlo_simulation(algorithm, "Lognormal", wtF[j], invwtF[j], -1, nX) / normalization)
			r.append(p)
		res2.append(r)

	print(res2)

	### save list of algorithm performance for all considered waiting time functions when stdev is shiffted by a given factor and mean is the original one ###

	f = open(getattr(settings,'path')+"plot_stdev.txt", 'w')
	json.dump(res2, f)
	f.close()


###### END Generates data of HPC experiment section with Intrepid data ######


































###### START Plot data of HPC experiment section with Intrepid data ######


### Plot the results for variance and mean variation with the different waiting time interpolation tested
if generates_HPC_plots_from_data:

	markers = ["s","o","D",">","<","*","h"]

	colors = ('black', 'midnightblue', 'r', 'c', 'm', 'y', 'k')


	number_points = 10 # number of points we want to plot in the data generated: plot_mean.txt, plot_stdev.txt (1<=number_points<=number_iterations)
	
	res = []
	res2 = []
	with open(getattr(settings,'path')+'plot_mean.txt') as f:
	    res = json.load(f)
	with open(getattr(settings,'path')+'plot_stdev.txt') as f:
	    res2 = json.load(f)


	colors = ['blue','red','magenta','darkorange','midnightblue','c','k']

	print("\n\nGenerating plots...")
	algorithms_str = ['{\sc Brute-Force}','{\sc Mean-by-Mean}','{\sc Mean-Stdev}','{\sc Mean-Doubling}', '{\sc Median-by-Median}' ,'{\sc Equal-time}','{\sc Equal-probability}']
	assert(len(colors)==len(algorithms_str))

	vect_mean=[i for i in range(1,number_points+1)]
	
	for j in range(0,len(wtF)):
		maxi = -1
		plt.figure()
		for i in range(0,len(res[j])):	
			#maxi = max(maxi, max(res[j][i]))
			maxi = max(maxi,max([res[j][i][k] for k in range(0,number_points)]))
			plt.plot([(mu*1253.370)/3600.0 for mu in vect_mean], [res[j][i][k] for k in range(0,number_points)] , colors[i], marker=markers[i], lw=3, alpha=0.6, label=r''+algorithms_str[i])
		plt.legend(loc='best', frameon=False, fontsize=12)
		plt.xlabel('Mean (hours)',fontsize=24)
		plt.ylabel('Normalized Expected Cost',fontsize=24)
		axes = plt.gca()
		axes.set_xlim([min([(mu*1253.370)/3600.0 for mu in vect_mean]),max([(mu*1253.370)/3600.0 for mu in vect_mean])])
		axes.set_ylim([0,maxi*1.1])
		plt.tick_params(axis='both', which='major', labelsize=18)
		plt.savefig(getattr(settings,'path')+wtF_str[j]+"_interpolation_distrib_mean.pdf",orientation='landscape')
		plt.close()

	
	for j in range(0,len(wtF)):
		maxi = -1
		plt.figure()
		for i in range(0,len(res2[j])):	
			#maxi = max(maxi, max(res2[j][i]))
			maxi = max(maxi,max([res2[j][i][k] for k in range(0,number_points)]))
			plt.plot([mu*258.261/(3600.0) for mu in vect_mean], [res2[j][i][k] for k in range(0,number_points)] , colors[i], marker=markers[i],lw=3, alpha=0.6, label=r''+algorithms_str[i])
		plt.legend(loc='best', frameon=False, fontsize=12)
		plt.xlabel('Standard Derivation (hours)',fontsize=24)
		plt.ylabel('Normalized Expected Cost',fontsize=24)
		axes = plt.gca()
		axes.set_xlim([min([mu*258.261/(3600.0) for mu in vect_mean]),max([mu*258.261/(3600.0) for mu in vect_mean])])
		axes.set_ylim([0,maxi*1.1])
		plt.tick_params(axis='both', which='major', labelsize=18)
		plt.savefig(getattr(settings,'path')+wtF_str[j]+"_interpolation_distrib_stdev.pdf",orientation='landscape')
		plt.close()


###### END Plot data of HPC experiment section with Intrepid data ######








print("Done\n\n\n\n\n******* End of simulation process *******\n\n")
