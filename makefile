# Copyright (c) 2018 Univ. Bordeaux


all: clean code

code:
	mkdir -p results
	python3 main.py

clean:
	rm -rf *.pyc __pycache__
