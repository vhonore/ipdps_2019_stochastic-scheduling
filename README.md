# Copyright (c) 2018-2022 Univ. Bordeaux #




# High Performance Stochastic Applications # 

This README file aims at presenting how to generate the simulation data of Inria RR-9211






### Getting Started ###

This repository contains all the code to reproduce the results obtained in Inria Research Report n° 9211. For convenience, the pdf of the RR is provided
(see RR-9211.pdf)

To run the code, a makefile is provided. Simple follow next steps to run and customize your tests.

Any question or issue can be adressed to valentin.honore@inria.fr






### Prerequisites ###

You will need to have python3 installed on your machine. The basic installation command would be like

```
sudo apt-get install python3-all
```

on mainstream Unix distributions.






### Running the simulations ###

Use the makefile at the root of the repository.

To run the tests:

```
make
```

This run the main.py file with the provided configuration. Please refer to next section of this readme to personnalize the tests.

Several data files are already provided:
* ```.tex```: contains the tabulars presented in the paper.
* ```.txt```: contains the data of the plots of Section V-C. This is given to avoid running the code that takes some time with paper settings.
* ```.pdf```: represents the plots provided in the paper. 



### Customizing your tests ###



Firstly, the code is tuned to generate the paper results. However, you can change the parameters at your convenience. We briefly explain here the structure of our programs and the main parameters you can change in each file.



###### Files ######

The simulator is divided into 5 files:

* 'main.py': main file that executes all the tests. User can choose the tests he wants to run
* 'distri.py': contains the distribution descriptions and features
* 'algorithms.py': constains the code of heuristics we considered in the paper
* 'settings.py': contains the main parameters of the simulator
* 'tools.py': contains various range of useful functions



###### 'main.py' ######

In this file, you can change the following parameters:

* ```generates_tables_data = True ``` -- If ```True```, generates the tables of the paper, in separated .tex files (Tables II,III and IV). This test takes few minutes in current setup.
* ```generates_pareto_precision_discretization = True ``` -- If ```True```, plots the experiments of changing the precision of the quantile for computing tmax of Pareto distribution and show the effect on discretization schemes (generates ```quantile_variation_discretization_pareto.pdf```). This test takes less than a minute in current setup.
* ```generates_HPC_simulation_data =  True ``` -- Generates the data files (.txt) of the plots of HPC evaluation section (V-C). This test takes around 2hours in current setup.
* ```generates_HPC_plots_from_data = True ``` -- Generates the plots associated to the data of .txt files created above (we provide the paper data files by default, ```plot_mean.txt``` and ```plot_stdev.txt```). This test takes few seconds in current setup.

Line 445, the variable ```nb_iterations = 30``` is used to fix the number of test you want to perform while generating data of Section V-C.

Line 653, the variable ```number_points = 10``` allows the user to select a number of points to plot from above experiments : ```0<nb_points<=nb_iterations```. The final plots will contain the points from the 1st one to the ```number_points^th``` one.
 


###### 'distri.py' ######

This file contains the definition of all distributions. You are free to provide your own distribution instanciations. However, we do not provide at the moment a way to add new distributions to the simulator.

Each distributiin is represented by its parameters, a variance, a mean and a ```min_distrib``` variable that contains the minimum value of the support. We consider distributions defined on positive support values.(```min_distrib = maximum(0,min_support)```).

The file also contains the PDF, CDF and inverse CDF function definitions of all distributions, as so as the quantile function of each distribution.



###### 'algorithms.py' ######

This file contains the code of the different algorithms we considered in the paper. No parameters are defined in this file.



###### 'settings.py' ######

This file contains the main parameters of the simulator. Here is the list and their meaning.

* 	 ```path=[results/]``` -- By default, all created files will be in ```./results/``` folder. You can change it at your convenience
*    ```alpha = 1.0``` -- Setup for cloud model (do not change)
*    ```gamma_ = 0.0``` -- Setup for cloud model (do not change)
*    ```beta = 0.0``` -- Cost function: WT(T)+ beta.min(X,T) (updated to 1.0 for experiments of Section V-C)
*    ```nX = 1000``` -- Number of iterations in the Monte-Carlo simulation
*    ```precision = 5000``` -- Precision of the evaluation for the brute-force algorithm (1/precision)
*    ```nb_chunks = [10,25,50,100,250,500,1000]``` -- List of number of chunks to test discretization
*    ```list_q = [0.25,0.5,0.75,0.99]``` -- List of different t1 we will try to evaluate recurrence relation
*    ```normalization_time = 3600``` -- Used to shift from second to hours in interpolations of HPC data
*    ```pthres = 0.99999999``` -- Threshold pmax value for discretization (the upperbound where we truncate)
*    ```max_exponential=quant_exponential(pthres)``` -- Maximum request for Exponential distribution
*    ```max_weibull = quant_weibull(pthres)``` -- Maximum request for Weibull distribution
*    ```max_gamma = quant_gamma(pthres)``` -- Maximum request for Gamma distribution
*    ```max_lognormal = quant_lognormal(pthres)``` -- Maximum request for Lognormal distribution
*    ```max_truncatednormal = quant_truncatednormal(pthres)``` -- Maximum request for TruncatedNormal distribution
*    ```max_pareto = quant_pareto(pthres)``` -- Maximum request for Pareto distribution
*    ```max_uniform = b_uniform``` -- Maximum request for Uniform distribution
*    ```max_beta = 1.0``` -- Maximum request for Beta distribution
*    ```max_boundedpareto = H_boundedpareto``` -- Maximum request for BoundedPareto distribution

This file also contains the waiting time functions that are used in the paper: linear (Section V-B) and interpolations from Intrepid (Section V-C)



###### 'tools.py' ######

This file contains the code of some useful functions (discretization schemes, brute-force procedure etc). No parameters are defined in this file.


