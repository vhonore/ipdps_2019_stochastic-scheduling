# Copyright (c) 2018 Univ. Bordeaux



import distri
from distri import *
import random


# Function used to initialize global variables of general settings
def init_settings():
    global alpha,beta,gamma_,nX,precision,nb_chunks,list_t,list_q, normalization_time, path
    alpha = 1.0 # Setup for cloud model
    gamma_ = 0.0 # Setup for cloud model
    beta = 0.0 # Cost function: WT(T)+ beta.min(X,T)
    nX = 1000 #number of iterations in the montecarlo simulation
    precision = 5000 #precision of the evaluation for the brute-force algorithm (1/precision)
    nb_chunks = [10,25,50,100,250,500,1000] ## list of number of chunks to test discretization
    list_q = [0.25,0.5,0.75,0.99] ## list of different t1 we will try for greedy
    list_t = [0.1,0.25,0.5,0.75,1,2] ## list of different t1 we will try for greedy
    normalization_time = 3600 # used to shift from second to hours in interpolations of HPC data
    random.seed(2*normalization_time+nX) # seed for reproducibility
    path = 'results/'

    global pthres, max_exponential, max_weibull, max_gamma, max_lognormal, max_truncatednormal, max_pareto, max_uniform, max_beta, max_boundedpareto
    pthres = 0.99999999 ## threshold pmax value for discretization (the upperbound where we truncate)
    max_exponential = quant_exponential(pthres) # maximum request for exponential distribution
    max_weibull = quant_weibull(pthres) # maximum request for weibull distribution
    max_gamma = quant_gamma(pthres) # maximum request for gamma distribution
    max_lognormal = quant_lognormal(pthres) # maximum request for lognormal distribution
    max_truncatednormal = quant_truncatednormal(pthres) # maximum request for truncated normal distribution
    max_pareto = quant_pareto(pthres) # maximum request for pareto distribution
    max_uniform = b_uniform # maximum request for uniform distribution
    max_beta = 1.0 # maximum request for beta distribution
    max_boundedpareto = H_boundedpareto # maximum request for boundedpareto distribution



# are we going with quantiles or times for greedy, by default quantiles
def qq():
	True


# computed the upper bound on the research of t1 opt for considered distribution
def t1_max(distrib):
    min_distrib = getattr(distri, "min_"+distrib.lower()) 
    mean_distrib = getattr(distri,"mean_"+distrib.lower()) 
    
    return (mean_distrib+1+((alpha+beta)/(2*alpha))*((getattr(distri, "variance_"+distrib.lower())) + pow(mean_distrib,2)-pow(min_distrib,2))+(alpha+beta+gamma_)*(mean_distrib-min_distrib)/alpha)





########################## START Waiting Time Functions ##########################
# WT(t) = t --> alpha = 1.0, gamma = 0.0
def linear(time):
	return time

def invlinear(time):
	return time




############### Intrepid data, 204 processors ###############

# normalization_time = used to shift time from second (if normalization_time = 1), to hours for example (normalization_time = 3600)
# can be changed in function init_settings() of settings.py

## Functions
def linear_interpolation_204(time):
	return 1.16*time - 1290.38/normalization_time

def exponential_interpolation_204(time):
    return 46625.61* pow(-46128.47,time) + (1.0/normalization_time)



## Inverse of the functions
def invlinear_interpolation_204(time):
	return (time+(1290.38/normalization_time))/1.16

def invexponential_interpolation_204(time):
    return sqrt(((time - (1.0/normalization_time))/46625.61),time)





############### Intrepid data, 409 processors ###############

## Functions

def linear_interpolation_409(time):
    return 0.95*time + 3771.84/normalization_time 

def quadratic_interpolation_409(time):
    return 0.00002*pow(time,2) + 9377.76/normalization_time


## Inverse of the functions

def invlinear_interpolation_409(time):
    return (time - (3771.84/normalization_time))/0.95

def invquadratic_interpolation_409(time):
    return sqrt((time-(9377.76/normalization_time))/0.00002)

