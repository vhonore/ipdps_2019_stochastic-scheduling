# Copyright (c) 2018 Univ. Bordeaux



from numpy import *
from scipy.stats import *
from scipy.special import *
from math import *







def phi(x):
	return ((1.0/sqrt(2.0*pi))*exp(-0.5*pow(x,2)))
def Phi(x):
	return (0.5*(1+erf(x/sqrt(2))))





############################# DISTRIBUTIONS PARAMETERS & MEAN/VARIANCE #############################


# Create and initialize all global variables necessary for algorithms and distribution representation #
def init_distributions():
	global lambda_exponential, mean_exponential, variance_exponential, min_exponential
	# Exponential ; t in [0;inf[
	lambda_exponential = 1.0 # > 0
	mean_exponential = 1.0/lambda_exponential
	variance_exponential = pow(lambda_exponential,-2)
	min_exponential = 0.0


	global mu_normal, sigma2_normal, mean_normal, variance_normal, min_normal
	# Normal ; t in ]-inf;inf[
	mu_normal = 8.0 # ]-inf;inf[
	sigma2_normal = 2.0 # > 0
	mean_normal = mu_normal
	variance_normal = sigma2_normal	
	min_normal = 0.0
	 

	global nu_lognormal, kappa2_lognormal, mean_lognormal, variance_lognormal, min_lognormal
	#LogNormal ; t in ]0;inf[
	nu_lognormal =  3# ]-inf,+inf[
	kappa2_lognormal = 0.25 # > 0
	mean_lognormal = exp(nu_lognormal + (kappa2_lognormal/2))
	variance_lognormal = (exp(kappa2_lognormal)-1)*(exp(2*nu_lognormal+kappa2_lognormal))
	min_lognormal = exp(nu_lognormal+(erfinv(2.0*0.00000000000001-1.0)*sqrt(2.0*kappa2_lognormal)))


	global lambda_weibull, kappa_weibull, mean_weibull, variance_weibull, min_weibull
	# Weibull ; ; t in [0;inf[
	lambda_weibull = 1.0 # [0,+inf[
	kappa_weibull = 0.5 # [0,+inf[
	mean_weibull = lambda_weibull * gamma(1+(1/kappa_weibull))
	variance_weibull = pow(lambda_weibull,2) * (gamma(1+(2/kappa_weibull))-pow(gamma(1+(1/kappa_weibull)),2))
	min_weibull = 0.0


	global alpha_gamma, beta_gamma, mean_gamma, variance_gamma, min_gamma
	# Gamma ; t in [0,inf[
	alpha_gamma = 2.0 # > 1
	beta_gamma = 2.0 # > 0
	mean_gamma = alpha_gamma/beta_gamma
	variance_gamma =  (alpha_gamma)/(pow(beta_gamma,2.0))
	min_gamma = 0.0


	global alpha_pareto, upsilon_pareto, mean_pareto, variance_pareto, min_pareto
	# Pareto ; t in [upsilon_pareto,inf[
	alpha_pareto = 3.0 # > 2 ; different of 1, greater than 2 for variance
	upsilon_pareto = 1.0/2.0*alpha_pareto  # upsilon_pareto < 1/alpha_pareto
	mean_pareto = (alpha_pareto*upsilon_pareto)/(alpha_pareto-1.0)
	variance_pareto = ((alpha_pareto*upsilon_pareto)/(pow(alpha_pareto-1,2.0)*(alpha_pareto-2.0)))
	min_pareto = upsilon_pareto


	global a_uniform, b_uniform, mean_uniform, variance_uniform, min_uniform
	# Uniform ; [a_uniform;b_uniform]
	a_uniform = 10.0 # ]-inf;inf[
	b_uniform = 20.0 # > a_uniform
	mean_uniform = (a_uniform+b_uniform)/2
	variance_uniform = (pow(b_uniform-a_uniform,2))/12 
	min_uniform = a_uniform


	global alpha_beta, beta_beta, mean_beta, variance_beta, min_beta
	# Beta ; t in [0;1]
	alpha_beta = 2.0 # > 0
	beta_beta = 2.0 # > 0
	mean_beta = alpha_beta/(alpha_beta+beta_beta)
	variance_beta = (alpha_beta*beta_beta)/(pow(alpha_beta+beta_beta,2)*(alpha_beta+beta_beta+1))
	min_beta = 0.0


	global L_boundedpareto, H_boundedpareto, alpha_boundedpareto, mean_boundedpareto, variance_boundedpareto, min_boundedpareto
	# BoundedPareto ; t in [L_boundedpareto;H_boundedpareto]
	L_boundedpareto = 1.0 # > 0
	H_boundedpareto = 20.0 # > 0
	alpha_boundedpareto = 2.1 # different of 2 (variance constraint)
	mean_boundedpareto = (pow(L_boundedpareto,alpha_boundedpareto)/(1-pow(L_boundedpareto/H_boundedpareto,alpha_boundedpareto))) * (alpha_boundedpareto/(alpha_boundedpareto-1)) * ((1/pow(L_boundedpareto,alpha_pareto-1)) - (1/pow(H_boundedpareto,alpha_boundedpareto-1))) 
	variance_boundedpareto = (pow(L_boundedpareto,alpha_boundedpareto)/(1-pow(L_boundedpareto/H_boundedpareto,alpha_boundedpareto))) * (alpha_boundedpareto/(alpha_boundedpareto-2)) * ((1/pow(L_boundedpareto,alpha_pareto-2)) - (1/pow(H_boundedpareto,alpha_boundedpareto-2))) 
	min_boundedpareto = L_boundedpareto


	global mu_truncatednormal, sigma2_truncatednormal, a_truncatednormal, alpha_truncatednormal, mean_truncatednormal, variance_truncatednormal, min_truncatednormal
	# truncatednormal ; t in [a;+inf[
	mu_truncatednormal = 8.0 # in R
	sigma2_truncatednormal = 2.0 # >= 1
	a_truncatednormal = 0.0 # >=0
	alpha_truncatednormal = (a_truncatednormal-mu_truncatednormal)/sqrt(sigma2_truncatednormal)
	mean_truncatednormal = mu_truncatednormal + (phi(alpha_truncatednormal)/(1-Phi(alpha_truncatednormal)))*sqrt(sigma2_truncatednormal)
	variance_truncatednormal = sigma2_truncatednormal * (1 + ((alpha_truncatednormal*phi(alpha_truncatednormal))/(1-Phi(alpha_truncatednormal))) - pow(phi(alpha_truncatednormal)/(1-phi(alpha_truncatednormal)),2) )
	min_truncatednormal = a_truncatednormal



# Initialize distributions 
init_distributions()






############################# DISTRIBUTIONS PDF/CDF/CDFr of distributions #############################
## Exponential distribution
def PDF_exponential(t):
	return lambda_exponential * exp(-lambda_exponential*t)

def CDF_exponential(t):
	return 1- exp(-lambda_exponential*t)

def CDFr_exponential(t):
	return 1.0-CDF_exponential(t)

def quant_exponential(quantile):
	return -(log(1-quantile))/lambda_exponential




## Normal Distribution ##
def PDF_normal(t):
	return ( (1.0/sqrt(2*pi*sigma2_normal)) * (exp(-(pow(t-mean_normal,2))/(2*sigma2_normal))))

def CDF_normal(t):
	return (0.5*(1.0+erf(((t-mean_normal)/(sqrt(sigma2_normal)*sqrt(2.0))))))

def CDFr_normal(t):
	return 1.0-CDF_normal(t)

def quant_normal(quantile):
	return mean_normal + sqrt(variance_normal)*sqrt(2)*erfinv(2*quantile-1) 



## Uniform Distribution ##
def PDF_uniform(t):
	if (a_uniform<=t and  t<=b_uniform):
		return (1.0/(b_uniform-a_uniform))
	else:
		return 0.0

def CDF_uniform(t):
	if (t<a_uniform):
		return 0.0
	elif (a_uniform<=t and t<b_uniform):
		return ((t-a_uniform)/(b_uniform-a_uniform))
	else:
		return 1.0
		
def CDFr_uniform(t):
	return 1-CDF_uniform(t)

def quant_uniform(quantile):
	return quantile*(b_uniform-a_uniform) + a_uniform



## Weibull Distribution ##
def PDF_weibull(t):
	if (t>=0):
		return ((kappa_weibull/lambda_weibull) * ((t/lambda_weibull)**(kappa_weibull-1)) * exp(-pow((t*1.0)/lambda_weibull,kappa_weibull)))
	else:
		return 0.0

def CDF_weibull(t):
	if (t>=0):
		return (1-exp(-pow(t/lambda_weibull,kappa_weibull)))
	else:
		return 0.0

def CDFr_weibull(t):
	return 1-CDF_weibull(t)
#real(cmath.exp((kaMissionsppa_weibull-1) * cmath.log(20/lambda_weibull)))

def quant_weibull(quantile):
	return lambda_weibull * pow(log(1.0/(1.0-quantile)),1.0/kappa_weibull)



## Lognormal Distribution ##
def PDF_lognormal(t):
	return ( (1.0/(t*sqrt(kappa2_lognormal)*sqrt(2*pi))) * (exp(-(pow(log(t)-nu_lognormal,2.0))/(2.0*kappa2_lognormal))) )

def CDF_lognormal(t):
	if (t<=0):
		return 0.0
	else:
		return ( 0.5 + 0.5*erf((log(t)-nu_lognormal)/(sqrt(2.0)*sqrt(kappa2_lognormal))) )

def CDFr_lognormal(t):
	return 1.0-CDF_lognormal(t)

def quant_lognormal(quantile):
	return exp(nu_lognormal+(erfinv(2.0*quantile-1.0)*sqrt(2.0)*sqrt(kappa2_lognormal)))




## Gamma Distribution ##
def PDF_gamma(t):
	return ( ((pow(beta_gamma,alpha_gamma))/(gamma(alpha_gamma))) * pow(t,alpha_gamma-1) * exp(-beta_gamma*t) )

def CDF_gamma(t):
	return ( gammainc(alpha_gamma,beta_gamma*t)/gamma(alpha_gamma) )

def CDFr_gamma(t):
	return 1.0-CDF_gamma(t)

def quant_gamma(quantile):
	return gammaincinv(alpha_gamma,quantile* gamma(alpha_gamma))/beta_gamma



## Pareto Distribution ##
def PDF_pareto(t):
	return ( ((alpha_pareto*pow(upsilon_pareto,alpha_pareto))/(pow(t,alpha_pareto+1))) )

def CDF_pareto(t):
	if (t<=upsilon_pareto):
		return 0.0
	else:
		return ( 1 - pow((upsilon_pareto/t),alpha_pareto) )

def CDFr_pareto(t):
	return 1.0-CDF_pareto(t)

def quant_pareto(quantile):
	return upsilon_pareto / (pow(1-quantile,1.0/alpha_pareto))


## Beta Distribution ##
def PDF_beta(t):
	return ( (pow(t,alpha_beta-1)*pow(1-t,beta_beta-1))/(beta(alpha_beta,beta_beta)) )

def CDF_beta(t):
	return ( betainc(alpha_beta,beta_beta,t) )

def CDFr_beta(t):
	return 1.0-CDF_beta(t)

def quant_beta(quantile):
	return betaincinv(alpha_beta, beta_beta, quantile)


## boundedpareto Distribution ##
def PDF_boundedpareto(t):
	return ( (alpha_boundedpareto*pow(L_boundedpareto,alpha_boundedpareto)*pow(t,-alpha_boundedpareto-1))/(1-pow(L_boundedpareto/H_boundedpareto,alpha_boundedpareto)) )

def CDF_boundedpareto(t):
	if (t<=0):
		return 0.0
	else:
		return ( (1-pow(L_boundedpareto,alpha_boundedpareto)*pow(t,-alpha_boundedpareto))/(1-pow(L_boundedpareto/H_boundedpareto,alpha_boundedpareto)) )

def CDFr_boundedpareto(t):
	return 1.0-CDF_boundedpareto(t)

def quant_boundedpareto(quantile):
	if (quantile==1):
		return H_boundedpareto
	else:
		return pow((1-quantile*(1-pow(L_boundedpareto/H_boundedpareto,alpha_boundedpareto))/(pow(L_boundedpareto,alpha_boundedpareto))),-1.0/alpha_boundedpareto)


## truncatednormal Distribution ##
def PDF_truncatednormal(t):
	return (phi((t-mu_truncatednormal)/sqrt(sigma2_truncatednormal))/(sqrt(sigma2_truncatednormal)*(1-Phi(alpha_truncatednormal))))

def CDF_truncatednormal(t):
	return (Phi((t-mu_truncatednormal)/sqrt(sigma2_truncatednormal))-Phi(alpha_truncatednormal))/(1-Phi(alpha_truncatednormal)) #( (erf((t-mu_truncatednormal)/(sqrt(sigma2_truncatednormal*2))) - erf((a_truncatednormal-mu_truncatednormal)/(sqrt(sigma2_truncatednormal*2)))) / (1-erf((a_truncatednormal-mu_truncatednormal)/(sqrt(sigma2_truncatednormal*2)))) )

def CDFr_truncatednormal(t):
	return 1.0-CDF_truncatednormal(t)

def quant_truncatednormal(quantile):
	return erfinv(quantile*(1-erf((a_truncatednormal-mu_truncatednormal)/sqrt(2*sigma2_truncatednormal)))+erf((a_truncatednormal-mu_truncatednormal)/sqrt(2*sigma2_truncatednormal)))*sqrt(2*sigma2_truncatednormal) + mu_truncatednormal






