import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib import rc
from copy import *
from cmath import *
import scipy
# Copyright (c) 2018 Univ. Bordeaux



import os


import distri
from distri import *
import settings



class NotStableException(Exception):
	pass







########################## START Application Time ##########################


# For a given distrib, a sample of distrib X and a sequence of requests result, compute the cost associated to result under ti<X for all ti in result
def cost_computation_stochastic(distrib,X,result,waitingTimeFunction):
	
	min_distrib = getattr(distri, "min_"+distrib.lower())
	max_distrib = getattr(settings, "max_"+distrib.lower())
	mean_distrib = getattr(distri,"mean_"+distrib.lower())

	ti1 = result[0]
	ti = result[1]

	cost = mean_distrib*settings.beta + waitingTimeFunction(ti)

	i = 2

	while (ti<X and i<len(result)):	
		ti1 = ti
		ti = result[i]
		cost += (waitingTimeFunction(ti) + settings.beta*ti1) 
		i+=1
	cost += X*settings.beta 
	
	return cost


########################## END Application Time ##########################













########################## START Monte-Carlo Simulation function ##########################


# Compute an expected cost form algorithm algorithm, with distrib distrib by Monte-Carlo simulation of len(nbX) samples
def monte_carlo_simulation(algorithm,distrib,waitingTimeFunction,invWaitingTimeFunction,t1,nbX,numberChunks=-1, discretization=-1):
	cost = 0.0

	# computation of Monte-Carlo samples
	vect_X = []
	for _ in range (0,nbX):
		X = 0.0
		quantile = random.uniform(0, 1) # Reproduciable
		X = globals()["quant_"+distrib.lower()](quantile)
		vect_X.append(X)
	
	# if dyn prog, precompute the sequence to avoid repeating the computation
	sequence_dyn_prog = []
	if (algorithm.__name__=="dynamic_programming_discrete"):
		assert(numberChunks>0 and (discretization==0 or discretization==1))
		sequence_dyn_prog = get_sequence_dynamic_programming(distrib, waitingTimeFunction, numberChunks, discretization)

	# Monte-Carlo process
	for X in vect_X:
		try:
			cost += algorithm(distrib, waitingTimeFunction, invWaitingTimeFunction, t1, X, sequence_dyn_prog, numberChunks, discretization)
			
		except NotStableException:
			return float("Inf")
	return cost/nbX
		

########################## END  Monte-Carlo Simulation function ##########################












########################## START Brute-Force Function ##########################


# Brute-Force procedure to find a couple (t1opt,costopt) of optimal recurrence relation by Monte-Carlo simulation for distrib distrib
# We test settings.precision t1 in the range (min_distrib,t1Max)	
# Printing is used to decide if we plot the function expected cost with regards to t1 (generating Figure 6 in Research Report)
def brute_force_simulation(algorithm,distrib,waitingTimeFunction,invWaitingTimeFunction,t1Max,nbX, discretization=-1,printing=1):
	iterations = getattr(settings,"precision")
	
	min_distrib = getattr(distri, "min_"+distrib.lower()) 
	max_distrib = getattr(settings, "max_"+distrib.lower())
	mean_distrib = getattr(distri,"mean_"+distrib.lower())
	assert(t1Max>min_distrib)	
	
	t = (min(max_distrib,t1Max)-min_distrib)/iterations

	best_t1 = float('Inf')
	best_cost = float('Inf')
	
	plot_list_t1 = []
	plot_list_exec_time = []

	for i in range(1,int(iterations)+1):
		input_t1 = min_distrib+i*t
		plot_list_t1.append(input_t1)
		if (input_t1>max_distrib):
			input_t1 = max_distrib
		try:
			# computation of Monte-Carlo samples
			vect_X = []
			for _ in range (0,nbX):
				X = 0.0
				quantile = random.uniform(0, 1) 
				X = globals()["quant_"+distrib.lower()](quantile)
				vect_X.append(X)
			cout = monte_carlo_simulation(algorithm, distrib,waitingTimeFunction,invWaitingTimeFunction, input_t1,nbX)
			plot_list_exec_time.append(cout)
			if (cout<best_cost):
				best_cost = cout
				best_t1 = input_t1
		except NotStableException:
			pass


	if (printing):
		plt.figure()
		plt.scatter(plot_list_t1, [val/mean_distrib for val in plot_list_exec_time],s=1,color='blue')
		axes = plt.gca()
		axes.set_xlim([0,t1Max*1.05])

		plt.xlabel(r'$t_1$',fontsize=24)
		plt.ylabel('Normalized Expected Cost',fontsize=24)
		if(distrib=="Uniform"):
			axes.set_ylim([1.33,1.34])
		plt.tick_params(axis='both', which='major', labelsize=18)
		plt.savefig(path+"cost_t1_"+distrib+".pdf",orientation='landscape')
		plt.close()

	return best_t1,best_cost
		

########################## END  Monte-Carlo Simulation function ##########################













########################## TOOLS TO DISCRETIZE A CONTINUOUS PROBABILITY DISTRIB ##########################


# Generate a liste of (ti,pi) following Equal-probability scheme
def discretization_proba(distrib, numberChunks):
	result = [[],[]] # structure to represent couples ti and pi
	for _ in range(0,numberChunks): 	# in this discretization, pi are the same
		result[1].append(1.0/numberChunks)

	tmax = getattr(settings, "max_"+distrib.lower())
	for i in range(1,numberChunks+1):
		t = 0.0
		# Exponential distribution
		if distrib == "Exponential":	
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			t = -(log(1-quantile))/getattr(distri,"lambda_exponential")
		# Lognormal distribution
		elif distrib == "Lognormal":
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			if (quantile==1):
				t = tmax
			else:
				t = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*quantile-1.0)*sqrt(2.0)*sqrt(getattr(distri,"kappa2_lognormal"))))	
		# Normal distribution
		elif distrib == "Normal": 
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			t = getattr(distri,"mean_normal") + sqrt(getattr(distri,"variance_normal"))*sqrt(2)*erfinv(2*quantile-1) 
		# Uniform distribution
		elif distrib == "Uniform":
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			t = quantile*(getattr(distri,"b_uniform")-getattr(distri,"a_uniform")) + getattr(distri,"a_uniform")
		# Weibull distribution
		elif distrib == "Weibull":
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			t = getattr(distri,"lambda_weibull") * pow(log(1.0/(1.0-quantile)),1.0/getattr(distri,"kappa_weibull"))
		# Gamma distribution
		elif distrib == "Gamma":
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			t = gammaincinv(getattr(distri,"alpha_gamma"),quantile*gamma(getattr(distri,"alpha_gamma")))/getattr(distri,"beta_gamma")
		# Pareto distribution
		elif distrib == "Pareto":
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			t = getattr(distri,"upsilon_pareto") / (pow(1-quantile,1.0/getattr(distri,"alpha_pareto"))) 
		# Beta distribution
		elif distrib == "Beta":
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			t = betaincinv(getattr(distri,"alpha_beta"), getattr(distri,"beta_beta"), quantile)
		# Bpareto distribution
		elif distrib == "BoundedPareto":
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			if (quantile==1):
				t = getattr(distri,"H_boundedpareto")
			else:
				t = pow((1-quantile*(1-pow(getattr(distri,"L_boundedpareto")/getattr(distri,"H_boundedpareto"),getattr(distri,"alpha_boundedpareto")))/(pow(getattr(distri,"L_boundedpareto"),getattr(distri,"alpha_boundedpareto")))),-1.0/getattr(distri,"alpha_boundedpareto"))
		elif (distrib=="TruncatedNormal"):
			quantile = (i/numberChunks)*globals()["CDF_"+distrib.lower()](tmax)
			t = erfinv(quantile*(1-erf((getattr(distri,"a_truncatednormal")-getattr(distri,"mu_truncatednormal"))/sqrt(2*getattr(distri,"sigma2_truncatednormal"))))+erf((getattr(distri,"a_truncatednormal")-getattr(distri,"mu_truncatednormal"))/sqrt(2*getattr(distri,"sigma2_truncatednormal"))))*sqrt(2*getattr(distri,"sigma2_truncatednormal")) + getattr(distri,"mu_truncatednormal")
		# ERROR
		else:
			print("ERROR: Wrong distribution name")
			exit(-1)
		
		# if t = inf, we keep previous non-inf value
		if (t==float('Inf')):
			t = deepcopy(result[0][len(result)-1])
		# if t > max_distrib, we set t to be max_distrib
		if (t>getattr(settings, "max_"+distrib.lower())):
			t = getattr(settings, "max_"+distrib.lower())
		assert(t>=0)

		
		result[0].append(deepcopy(t))
	
	return result


# Generate a liste of (ti,pi) following Equal-time scheme
def discretization_time(distrib, numberChunks):
	result = [[],[]] # list of ti, list of pi
	
	tmax = getattr(settings, "max_"+distrib.lower())
	tmin = getattr(distri, "min_"+distrib.lower())
  
	for i in range(1,numberChunks+1):
		tmp = tmin +(i*((tmax-tmin)/numberChunks))
		result[0].append(tmp)

	result[1].append((globals()["CDF_"+distrib.lower()](result[0][0])-globals()["CDF_"+distrib.lower()](tmin))/globals()["CDF_"+distrib.lower()](tmax))
	
	for i in range(1,numberChunks):
		result[1].append((globals()["CDF_"+distrib.lower()](result[0][i])-globals()["CDF_"+distrib.lower()](result[0][i-1]))/globals()["CDF_"+distrib.lower()](tmax))

	return result


########################## END TOOLS TO DISCRETIZE A CONTINUOUS PROBABILITY DISTRIB ##########################













########################## START Dynamic programming Algorithm  ##########################


# Compute a sequence of requests for a continuous distribution given a number of chunks
# discretization = 1 --> Equal-Time scheme
# discretization = 0 --> Equal-Probability scheme
def get_sequence_dynamic_programming(distrib, waitingTimeFunction, numberChunks, discretization=1):

	min_distrib = getattr(distri, "min_"+distrib.lower())#globals()["min_"+distrib.lower()]  

	
	data = [min_distrib]
	if (discretization):
		data = discretization_time(distrib,numberChunks)
	else:
		data = discretization_proba(distrib,numberChunks)

	expt_t = []
	assert( len(data[0])==numberChunks and len(data[1])==numberChunks)
	assert(numberChunks)
	
	n = numberChunks-1



	expt_t = []
	tim = waitingTimeFunction(data[0][n])
	tim += settings.beta*data[0][n]
	expt_t.append(tim)
	
	jstar = [n]
	for i in range(n-1,-1,-1): 
		opt_j = 0.0
		t = waitingTimeFunction(data[0][i])
		sum_adj_proba = 0.0
		for l in range(i,n+1):
			sum_adj_proba = sum_adj_proba + data[1][l]
		if(not sum_adj_proba):
			sum_adj_proba = 1.0
		t = t + (data[1][i]/sum_adj_proba)*(settings.beta*data[0][i])
		cpt = 0.0
		for k in range(i+1,n+1):
			cpt = cpt +(data[1][k]/sum_adj_proba)
		t = t + (cpt *(settings.beta*data[0][i] + expt_t[n-i-1]))
		opt_j = deepcopy(i)
		for j in range(i+1,n+1):
			r = 0.0
			r  = waitingTimeFunction(data[0][j])
			for k in range(i,j+1):
				r = r + (data[1][k]/sum_adj_proba)*settings.beta*data[0][k]
			cpt = 0.0
			if(j!=n):
				for k in range(j+1,n+1):
					cpt = cpt + (data[1][k]/sum_adj_proba)
				r = r + (cpt *(settings.beta*data[0][j] + expt_t[n-j-1])) 
			if (r<t):
				opt_j = deepcopy(j)
				t = deepcopy(r)
		jstar.append(opt_j)
		expt_t.append(t)
	
 	# backtracking
	jstar = jstar[::-1]
	expt_t = expt_t[::-1]

	liste = []
	current_ind = jstar[0]
	while(current_ind<n):
		liste.append(current_ind)
		current_ind = jstar[current_ind+1]
	liste.append(n)

	
	result = [min_distrib]
	for i in liste:
		result.append(data[0][i])
	
	return result


########################## END Dynamic programming Algorithm ##########################