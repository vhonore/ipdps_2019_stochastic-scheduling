# Copyright (c) 2018 Univ. Bordeaux



import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib import rc
from copy import *
from cmath import *
import scipy
import os

import settings

import distri
from tools import *

rc('text', usetex=True)





####### LIST OF ALGORITHMS HERE:
## Continuous
#def mean_by_mean(distrib, waitingTimeFunction, invWaitingTimeFunction, settings.beta, t1, X, numberChunks=-1, discretization=-1):
#def mean_variance(distrib, waitingTimeFunction, invWaitingTimeFunction, settings.beta, t1, X, numberChunks=-1, discretization=-1):
#def mean_doubling(distrib, waitingTimeFunction, invWaitingTimeFunction, settings.beta, t1, X, numberChunks=-1, discretization=-1):
#def medium_by_medium(distrib, waitingTimeFunction, invWaitingTimeFunction, settings.beta, t1, X, numberChunks=-1, discretization=-1):
#def optimal_recurrence_stochastic(distrib, waitingTimeFunction, invWaitingTimeFunction, settings.beta, t1, X, numberChunks=-1, discretization=-1):
## Using discretization tools (available in tools.py)
#def dynamic_programming_discrete(distrib, waitingTimeFunction, invWaitingTimeFunction, settings.beta, t1, X, numberChunks, discretization=0):




########################## START Algorithms ##########################








def mean_by_mean(distrib, waitingTimeFunction, invWaitingTimeFunction, t1, X, result, numberChunks=-1, discretization=-1):
	
	
	min_distrib = getattr(distri, "min_"+distrib.lower())
	max_distrib = getattr(settings, "max_"+distrib.lower())
	mean_distrib = getattr(distri,"mean_"+distrib.lower())


	result = [min_distrib]
	
	t = 0.0
	# Exponential distribution
	if distrib == "Exponential":
		t = getattr(distri,"mean_exponential")
		result.append(getattr(distri,"mean_exponential"))
		while(t<X):
			t += getattr(distri,"mean_exponential")
			result.append(t)
	# Uniform distribution
	elif distrib == "Uniform":
		t = getattr(distri,"mean_uniform")
		result.append(t)
		while(t<X):
			r = 0.5*(getattr(distri,"b_uniform")+t)
			result.append(deepcopy(r))
			t = deepcopy(r)
	# Weibull distribution
	elif distrib == "Weibull":
		R = gamma(1.0+(1.0/getattr(distri,"kappa_weibull")))
		t = R*getattr(distri,"lambda_weibull")
		result.append(getattr(distri,"mean_weibull"))
		while(t<X):
			R = exp(pow(R,getattr(distri,"kappa_weibull"))) * gammaincc(1.0+(1.0/getattr(distri,"kappa_weibull")),pow(R,1.0*getattr(distri,"kappa_weibull")))*gamma(1.0+(1.0/getattr(distri,"kappa_weibull"))) #note that gammaincc is normalized.
			ti = t
			t = getattr(distri,"lambda_weibull") * R
			if (ti+0.000001>=t):
				break
			result.append(t)
    # Lognormal distribution
	elif distrib == "Lognormal":
		R = 1
		result.append(getattr(distri,"mean_lognormal"))#exp(getattr(distri,"nu_lognormal")+(getattr(distri,"kappa2_lognormal")/2)))
		while(t<X):
			R = ( (1+erf((getattr(distri,"kappa2_lognormal")-2*log(R))/(2*sqrt(2*getattr(distri,"kappa2_lognormal"))))) / (1-erf((getattr(distri,"kappa2_lognormal")+2*log(R))/(2*sqrt(2*getattr(distri,"kappa2_lognormal"))))) )
			t = getattr(distri,"mean_lognormal") * R #exp(getattr(distri,"nu_lognormal")+(getattr(distri,"kappa2_lognormal")/2)) * R
			result.append(t)
	# Gamma distribution
	elif distrib == "Gamma":
		t = getattr(distri,"mean_gamma")
		R = getattr(distri,"alpha_gamma")
		result.append(getattr(distri,"mean_gamma"))
		while(t<X):
			R = getattr(distri,"alpha_gamma") + ((pow(R,getattr(distri,"alpha_gamma"))*exp(-R))/(gammaincc(getattr(distri,"alpha_gamma"),R)*gamma(getattr(distri,"alpha_gamma"))))
			t = (1.0/getattr(distri,"beta_gamma"))*R
			if (t<=result[len(result)-1]):
				break
			result.append(deepcopy(t))
	# Pareto distribution
	elif distrib == "Pareto":
		t = getattr(distri,"mean_pareto")
		result.append(t)
		while(t<X):
			t = (getattr(distri,"alpha_pareto")/(getattr(distri,"alpha_pareto")-1.0))*t
			result.append(deepcopy(t))
	elif distrib == "Beta":
		t = getattr(distri,"mean_beta")
		result.append(t)
		while(t<X):
			t = (scipy.special.beta(getattr(distri,"alpha_beta")+1,getattr(distri,"beta_beta"))-betainc(t,getattr(distri,"alpha_beta")+1,getattr(distri,"beta_beta"))) / (scipy.special.beta(getattr(distri,"alpha_beta"),getattr(distri,"beta_beta"))-betainc(t,getattr(distri,"alpha_beta"),getattr(distri,"beta_beta")))
			if (t>=max_distrib or isnan(t) or isinf(t)):
				t = max_distrib
			result.append(deepcopy(t))
	# Bpareto distribution
	elif distrib == "BoundedPareto":
		t = getattr(distri,"mean_boundedpareto")
		result.append(getattr(distri,"mean_boundedpareto"))
		while(t<X):
			t = (getattr(distri,"alpha_boundedpareto")/(getattr(distri,"alpha_boundedpareto")-1)) * ( (pow(getattr(distri,"H_boundedpareto"),1-getattr(distri,"alpha_boundedpareto")) - pow(t,1-getattr(distri,"alpha_boundedpareto"))) / (pow(getattr(distri,"H_boundedpareto"),-getattr(distri,"alpha_boundedpareto"))- pow(t,-getattr(distri,"alpha_boundedpareto"))) )
			result.append(deepcopy(t))
	elif distrib == "TruncatedNormal":
		R = 1
		t = 0.0
		while(t<X):
			var = 0.0
			try:
				if (R==1):
					var = exp(-0.5*pow((getattr(distri,"a_truncatednormal")-getattr(distri,"mu_truncatednormal"))/sqrt(getattr(distri,"sigma2_truncatednormal")),2)) / (1-erf((getattr(distri,"a_truncatednormal")-getattr(distri,"mu_truncatednormal"))/sqrt(2*getattr(distri,"sigma2_truncatednormal"))))
				else:
					var = exp(-R/pi) / (1-erf(R/sqrt(pi)))
			except ZeroDivisionError:
				break
			R = deepcopy(var)	
			t = getattr(distri,"mean_truncatednormal") + sqrt(getattr(distri,"variance_truncatednormal"))*sqrt(2/pi)*R
			result.append(t)

	# ERROR
	else:
		print("ERROR: Wrong distribution name")
		exit(-1)
	
	return cost_computation_stochastic(distrib,X,result,waitingTimeFunction)








def mean_variance(distrib, waitingTimeFunction, invWaitingTimeFunction, t1, X, result, numberChunks=-1, discretization=1):
	result = [getattr(distri,"min_"+distrib.lower())]
	
	max_distrib = getattr(settings, "max_"+distrib.lower())
	t = getattr(distri,"mean_"+distrib.lower())
	result.append(t)
	while (t<X):
		try:
			t += sqrt(getattr(distri,"variance_"+distrib.lower()))
		except ValueError:
			t = max_distrib
		result.append(t)

	return cost_computation_stochastic(distrib,X,result,waitingTimeFunction)








def mean_doubling(distrib, waitingTimeFunction, invWaitingTimeFunction, t1, X, result, numberChunks=-1, discretization=-1):
	result = [getattr(distri,"min_"+distrib.lower())]
	max_distrib = getattr(settings, "max_"+distrib.lower()) 

	t = getattr(distri,"mean_"+distrib.lower())
	result.append(t)

	while(t<X):
		t = 2 * t
		if (t>max_distrib):
			t = deepcopy(max_distrib)
		result.append(deepcopy(t))
	
	return cost_computation_stochastic(distrib,X,result,waitingTimeFunction)








def medium_by_medium(distrib, waitingTimeFunction, invWaitingTimeFunction, t1, X, result, numberChunks=-1, discretization=-1):
	result = [getattr(distri,"min_"+distrib.lower())]
	max_distrib =  getattr(settings, "max_"+distrib.lower()) 

	min_distrib = getattr(distri, "min_"+distrib.lower())

	quantile = 0.5
	t = getattr(distri,"min_"+distrib.lower())
	while(t<X):
		t = globals()["quant_"+distrib.lower()](quantile)
		quantile += (1.0-quantile)/2.0
		if (t>max_distrib):
			t=max_distrib
		result.append(deepcopy(t))

	if(len(result)==1):
		result.append(min_distrib)
		

	return cost_computation_stochastic(distrib,X,result,waitingTimeFunction)








#If discretization, discretization with time. Else with proba
def dynamic_programming_discrete(distrib, waitingTimeFunction, invWaitingTimeFunction, t1, X, result, numberChunks, discretization):
	assert(result)
	return cost_computation_stochastic(distrib,X,result,waitingTimeFunction)








# Optimal recurrence that is computed with t0 = min_distrib and t1 = t1 given in parameter
def optimal_recurrence_stochastic(distrib, waitingTimeFunction, invWaitingTimeFunction, t1, X, result, numberChunks=-1, discretization=-1):
	max_distrib = getattr(settings, "max_"+distrib.lower())
	min_distrib = getattr(distri,"min_"+distrib.lower())	
	

	result = [0,min(t1,max_distrib)]

	ti1 = result[0]
	ti = result[1]

	while (X>ti):
		ti2 = ti1
		ti1 = ti
		ti = min(max_distrib,invWaitingTimeFunction(waitingTimeFunction(globals()["CDFr_"+distrib.lower()](ti2)/((globals()["PDF_"+distrib.lower()](ti1)))) + settings.beta * (globals()["CDFr_"+distrib.lower()](ti1)/(globals()["PDF_"+distrib.lower()](ti1)) - ti1)))
		if (ti<=ti1):
			raise NotStableException()
		result.append(ti)
	c = cost_computation_stochastic(distrib,X,result,waitingTimeFunction)
	return c








########################## END Algorithms ##########################




















